<?php
session_start();

$user_id = $_SESSION['user_id'];

include "../classes/User.php";

use classes\User;

if(!isset($_POST['sure'])) {
    header("Location: ../index.php");
}

User::deleteAllPosts($user_id);

header("Location: ../myPosts.php");