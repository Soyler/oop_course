<?php

session_start();

include "../classes/User.php";
include "../classes/UserValidator.php";

use classes\User;
use classes\UserValidator;

if(!empty($_POST)) {

//  Получаем данные из формы
    $login = $_POST['login'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $rePassword = $_POST['password_confirmation'];

//  Валидируем данные через наш класс Validator
    $result = UserValidator::validate($email, $password, $rePassword);
    if(!is_null($result)) {
        foreach ($result as $error) {
            echo $error."<br>";
        }
        echo "<br><a href='../login_register_modal.php'><b>Вернуться к регистрации</b></a>";
        die();
    }

//  Создаем объект пользователя
    $user = new User($login, $email,$password,$rePassword);
    if($user->registration()) {
        $_SESSION['user_login'] = $user->getLogin();
        $_SESSION['user_id'] = $user->getId();
        header('Location: ../index.php');
    }
    else {
        echo 'Не удалось зарегистрироваться. Возможно, пользователь с таким Login или E-mail уже существует.';
        echo "<br><a href='../login_register_modal.php'><b>Вернуться к регистрации</b></a>";
        die();
    }
//    echo $user->register();

}
else {
    header('Location: ../login_register_modal.php');
}