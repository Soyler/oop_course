<?php

include "../classes/User.php";

use classes\User;

if( isset( $_POST['edit_post'] ) )
{

    $post_id = $_POST['post_id'];
    $newTitle = $_POST['title'];
    $newContent = $_POST['content'];

    User::editPostById($post_id, $newTitle, $newContent);
}

header("Location: ../myPosts.php");
