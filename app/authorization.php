<?php

session_start();

include "../classes/User.php";

use classes\User;

if(!empty($_POST)) {

    $email = $_POST['email'];
    $password = $_POST['password'];

    $user = new User(null, $email, $password, null);
    if($user->authorization()) {
        $_SESSION['user_login'] = $user->authorization();
        $_SESSION['user_id'] = $user->getId();

        header('Location: ../index.php');
    }
    else {
        die('Вы ввели неверные данные.');
    }

}