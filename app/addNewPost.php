<?php

include "../classes/Posts.php";

use classes\Posts;

if(!isset($_POST)) {
    header("Location: ../index.php");
}

$title = $_POST['title'];
$content = $_POST['content'];
$user_id = $_POST['user_id'];

$post = new Posts($title, $content, $user_id);
$post->savePost();

header("Location: ../myPosts.php");


