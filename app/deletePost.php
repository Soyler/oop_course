<?php

include "../classes/User.php";

use classes\User;

if( isset( $_POST['delete_post'] ) )
{
    $post_id = $_POST['post_id'];
    User::deletePostById($post_id);
}

header("Location: ../myPosts.php");

?>