<?php

namespace classes;

include 'Database.php';
include 'FinalQuery.php';
include 'InterfaceUser.php';

use classes\Database;
use classes\FinalQuery;
use classes\InterfaceUser;

class User implements InterfaceUser
{
    private $login;
    private $email;
    private $password;
    private $rePassword;

    public function __construct($login, $email, $password, $rePassword)
    {
        $this->login = $login;
        $this->email = $email;
        $this->password = $password;
        $this->rePassword = $rePassword;
    }


    public function registration()
    {
//        echo 'Вы успешно зарегистрировались!';
        $db = new Database();
        $query = "INSERT INTO `users`(`login`, `email`, `password`) VALUES ('" . $this->login . "','" . $this->email . "','" . $this->password . "')";
        return $db->query($query);
    }

    public function authorization()
    {
        $db = new Database();
        $query = "SELECT * FROM users WHERE `email` = '" . $this->email . "' AND `password` = '" . $this->password . "'";
        $result = $db->query($query);
        if ($result->num_rows !== 0) {
            $row = mysqli_fetch_assoc($result);
            $this->login = $row['login'];
            return $this->login;
        } else {
            return false;
        }
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getId()
    {
        $db = new Database();
        if ($this->login == null) {
            $email = $this->email;
            $query = FinalQuery::getUserIdByEmail($email);
//            $query = "SELECT `id` FROM users WHERE `email` = '" . $this->email . "'";
        } else {
            $login = $this->login;
            $query = FinalQuery::getUserIdByLogin($login);
//            $query = "SELECT `id` FROM users WHERE `login` = '" . $this->login . "'";
        }
        $result = $db->query($query);
        if ($result->num_rows !== 0) {
            $row = mysqli_fetch_assoc($result);
            $id = $row['id'];
            return $id;
        } else {
            return false;
        }
    }

    public static function deleteAllPosts($user_id)
    {
        $db = new Database();
//        $query = "DELETE FROM `posts` WHERE `user_id` = '" . $user_id . "'";
        $query = FinalQuery::deleteAllPosts($user_id);
        $db->query($query);
    }

    public static function deletePostById($post_id)
    {
        $db = new Database();
//        $query = "DELETE FROM `posts` WHERE `id` = '" . $post_id . "'";
        $query = FinalQuery::deletePostById($post_id);
        $db->query($query);
    }

    public static function editPostById($post_id, $newTitle, $newContent)
    {
        $db = new Database();
        $query = "UPDATE `posts` SET `title`='".$newTitle."',`content`='".$newContent."' WHERE `id` = '".$post_id."'";
        $db->query($query);
    }
}