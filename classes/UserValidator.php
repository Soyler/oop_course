<?php


namespace classes;

abstract class UserValidator
{

    private static function validateEmail($email) {
        return (preg_match("/[0-9a-z]+@[a-z]{1,10}\.[a-z]{1,10}/", $email)) ? TRUE : FALSE;
    }

    private static function validatePassword($password, $rePassword) {
        return ($password == $rePassword) ? TRUE : FALSE;
    }

    public static function validate($email, $password, $rePassword) {
        $errors = [];
        (self::validateEmail($email)) ? : $errors[] = 'Неверный e-mail';
        (self::validatePassword($password, $rePassword)) ? : $errors[] = 'Пароли не совпадают';

        return ($errors) ? $errors : null;
    }
}