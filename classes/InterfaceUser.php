<?php

namespace classes;

interface InterfaceUser {
    public function registration();
    public function authorization();
}