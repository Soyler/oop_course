<?php

namespace classes;

class Database
{
    protected $connection;
    protected $query;

    public function __construct($dbhost = 'localhost', $dbuser = 'root', $dbpass = 'root', $dbname = 'phpbook')
    {
//        $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
        $this->connection = new \mysqli($dbhost,$dbuser,$dbpass,$dbname);
        if ($this->connection->connect_errno) {
            printf("Не удалось подключиться: %s\n", $this->connection->connect_error);
            exit();
        }
    }

    public function query($query)
    {
        $this->query = $query;
        return $this->connection->query($this->query);
    }
}