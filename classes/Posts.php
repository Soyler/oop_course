<?php

namespace classes;

include 'Database.php';
include 'FinalQuery.php';

use classes\Database;
use classes\FinalQuery;

class Posts
{
    private $title;
    private $content;
    private $author;

    public function __construct($title, $content, $author)
    {
        $this->title = $title;
        $this->content = $content;
        $this->author = $author;
    }

    public static function getAllPosts()
    {
        $db = new Database();
//        $query = "SELECT * FROM posts";
        $query = FinalQuery::getAllPosts();
        return $posts = $db->query($query);
    }

    public static function getUserPosts($user_id)
    {
        $db = new Database();
//        $query = "SELECT * FROM posts WHERE `user_id` = '".$user_id."'";
        $query = FinalQuery::getUserPostsById($user_id);
        return $posts = $db->query($query);
    }

    public function savePost()
    {
        $db = new Database();
        $query = "INSERT INTO `posts`(`user_id`, `title`, `content`) VALUES ('".$this->author."','".$this->title."','".$this->content."')";
        return $db->query($query);
    }

    public static function getAuthorById($user_id)
    {
        $db = new Database();
//        $query = "SELECT login FROM users WHERE `id` = '".$user_id."' LIMIT 1";
        $query = FinalQuery::getUserLoginByUserId($user_id);
        $result = $db->query($query);
        if($result->num_rows !== 0) {
            $row = mysqli_fetch_assoc($result);
            $login = $row['login'];
            return $login;
        }
        else {
            return false;
        }
    }

}