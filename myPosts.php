<?php session_start(); ?>
<?php include "header.html"; ?>
<?php $user_id = $_SESSION['user_id']; ?>
<div>
    <input type="button" value="Добавить статью" id="create_post_button">
    <input type="button" value="Удалить все мои статьи" id="delete_posts_button">
</div>
<div id="create_post_div" style="display: none;">
    <form method="post" action="app/addNewPost.php">
        <p>Добавить новую статью:</p>
        <input type="text" name="title" placeholder="Заголовок">
        <input type="text" name="content" placeholder="Содержимое">
        <input type="hidden" name="user_id" value="<?=$user_id?>">
        <input type="submit">
    </form>
</div>
<div id="delete_posts_div" style="display: none;">
    <p>Вы уверены что хотите удалить все статьи?</p>
    <p>Это действие отменить невозможно!</p>
    <form method="post" action="app/deleteAllPosts.php">
        <input type="hidden" name="sure" value="yes">
        <input type="submit" value="Удалить все статьи">
    </form>
</div>

<hr>
<?php

include "classes/Posts.php";

use classes\Posts;

if($posts = Posts::getUserPosts($user_id)) {
}
else {
    echo 'не удалось получить посты';
}

foreach($posts as $post) {?>
    <article>
        <h2>
            <span><?=$post['title']?></span> <u>(Автор: <?=Posts::getAuthorById($post['user_id']);?>)</u>
        </h2>
        <div>
            <p><?=$post['content']?></p>
        </div>
        <div style="display: grid; grid-gap: 15px;">
            <form method="POST" action="app/editPost.php">
                <input type="text" name="title" placeholder="New Title" value="<?=$post['title']?>">
                <input type="text" name="content" placeholder="New Content" value="<?=$post['content']?>">
                <input type="submit" name="edit_post" value="Редактировать статью" />
                <input type="hidden" name="post_id" value="<?=$post['id']?>">
            </form>
            <form method="POST" action="app/deletePost.php">
                <input type="submit" name="delete_post" value="Удалить статью" />
                <input type="hidden" name="post_id" value="<?=$post['id']?>">
            </form>
        </div>
    </article>
    <hr>
    <?php
}

?>

<?php include "footer.html"; ?>
