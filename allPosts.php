<?php session_start(); ?>
<?php include "header.html"; ?>

<?php

include "classes/Posts.php";

use classes\Posts;

if($posts = Posts::getAllPosts()) {
//    echo 'Статьи успешно получены';
}
else {
    echo 'Не удалось получить статьи';
}

foreach($posts as $post) {?>
    <article>
        <h2>
            <span><?=$post['title']?></span> <u>(Автор: <?=Posts::getAuthorById($post['user_id']);?>)</u>
        </h2>
        <div>
            <p><?=$post['content']?></p>
        </div>
    </article>
    <hr>
<?php
}

?>

<?php include "footer.html"; ?>
