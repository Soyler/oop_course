let create_post_button = document.getElementById('create_post_button');
let create_post_div = document.getElementById('create_post_div');

create_post_button.addEventListener('click', function () {
   if(create_post_div.classList.contains('visible')) {
       create_post_div.style.display = "none";
       create_post_div.classList.remove('visible');
   }
   else {
       create_post_div.style.display = "block";
       create_post_div.classList.add('visible');
   }
});

let delete_posts_button = document.getElementById('delete_posts_button');
let delete_posts_div = document.getElementById('delete_posts_div');

delete_posts_button.addEventListener('click', function () {
    if(delete_posts_div.classList.contains('visible')) {
        delete_posts_div.style.display = "none";
        delete_posts_div.classList.remove('visible');
    }
    else {
        delete_posts_div.style.display = "block";
        delete_posts_div.classList.add('visible');
    }
});